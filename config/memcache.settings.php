<?php

use Drupal\Core\Site\Settings;
use Drupal\memcache\Cache\TimestampCacheTagsChecksum;
use Drupal\memcache\Driver\MemcacheDriverFactory;
use Drupal\memcache\DrupalMemcacheInterface;
use Drupal\memcache\Invalidator\MemcacheTimestampInvalidator;
use Drupal\memcache\MemcacheBackend;
use Drupal\memcache\MemcacheSettings;

$settings['memcache']['servers'] = [
  'memcached:11211' => 'default',
];

$settings['bootstrap_container_definition'] = [
  'parameters' => [],
  'services' => [
    'settings' => [
      'class' => Settings::class,
      'factory' => 'Drupal\Core\Site\Settings::getInstance',
    ],
    'memcache.settings' => [
      'class' => MemcacheSettings::class,
      'arguments' => ['@settings'],
    ],
    'memcache.factory' => [
      'class' => MemcacheDriverFactory::class,
      'arguments' => ['@memcache.settings'],
    ],
    'memcache.timestamp.invalidator.bin' => [
      'class' => MemcacheTimestampInvalidator::class,
      'arguments' => ['@memcache.factory', 'memcache_bin_timestamps', 0.001],
    ],
    'memcache.timestamp.invalidator.tag' => [
      'class' => MemcacheTimestampInvalidator::class,
      'arguments' => ['@memcache.factory', 'memcache_tag_timestamps', 0.001],
    ],
    'memcache.backend.cache.container' => [
      'class' => DrupalMemcacheInterface::class,
      'factory' => ['@memcache.factory', 'get'],
      'arguments' => ['container'],
    ],
    'cache_tags_provider.container' => [
      'class' => TimestampCacheTagsChecksum::class,
      'arguments' => ['@memcache.timestamp.invalidator.tag'],
    ],
    'cache.container' => [
      'class' => MemcacheBackend::class,
      'arguments' => [
        'container',
        '@memcache.backend.cache.container',
        '@cache_tags_provider.container',
        '@memcache.timestamp.invalidator.bin',
      ],
    ],
  ],
];
