require('./jquery.flexslider.js');

(function ($) {
  "use strict";

  Drupal.behaviors.nsr = {
    attach: function () {

      const flexslider = $('.flexslider');
      if (flexslider.length > 0) {
        if (!flexslider.data('flexslider-initialized')) {
          flexslider.flexslider();
          flexslider.data('flexslider-initialized', true);
        }
      }

      const body = $('body'),
        mainMenu = $('.main-menu'),
        menu = mainMenu.find('.menu'),
        menuToggle = mainMenu.find('.menu-toggle');

      mainMenu.on('click', '.menu-toggle', function (e) {
        e.preventDefault();
        menu.slideToggle('fast');
      });

      mainMenu.on('click', '.toggle-submenu-js', function (e) {
        e.preventDefault();
        if (menuToggle.css('display') !== 'block') {
          return;
        }
        const parent = $(this).parent(),
          submenu = parent.find('ol'),
          plus = parent.find('i.plus'),
          min = parent.find('i.min');

        if (submenu.css('display') === 'block') {
          submenu.slideUp('fast');
          plus.fadeIn('fast');
          min.fadeOut('fast');
        }
        else {
          if (submenu.css('display') === undefined) {
            window.open($(this).attr("href"), "_top");
          }
          else {
            menu.find('ol').slideUp('fast');
            menu.find('i.plus').fadeIn('fast');
            min.fadeIn('fast');
            plus.fadeOut('fast');
            submenu.slideDown('fast');
          }
        }
      });

      body.on('click', '.toggle-footer-links', function (e) {
        e.preventDefault();
        if (menuToggle.css('display') === 'block') {
          $('.footer-link-container').slideToggle();
        }
      });

      // Open search bar
      body.on('click', '.search-toggle-js', function () {
        mainMenu.toggleClass('search-open');
        mainMenu.find('.search-container input.search').select().focus();
      });

      // Close search bar
      body.on('click', '.close-suggestion-list-js', function () {
        mainMenu.find('.suggestion-list').hide();
        mainMenu.removeClass('search-open');
      });

      body.on('click', '.up', function (e) {
        e.preventDefault();
        window.scrollTo(0, 0);
      });
    }
  };
})(jQuery);
