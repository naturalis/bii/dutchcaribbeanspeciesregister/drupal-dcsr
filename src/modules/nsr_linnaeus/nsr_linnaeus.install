<?php

use Drupal\Core\Language\LanguageInterface;
use Drupal\nsr_linnaeus\Plugin\Block\LinnaeusBlock;
use Drupal\nsr_linnaeus\ValueObject\LastImagesData;
use Drupal\nsr_linnaeus\ValueObject\StatisticsData;

/**
 * Implements hook_install().
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
function nsr_linnaeus_install(): void {
  $statisticsData = [
    'categories' => [
      'data' => [
        0 => [
          'count' => '5.198',
          'label' => 'marine',
        ],
        1 => [
          'count' => '4.235',
          'label' => 'terrestrial/freshwater/brackish water',
        ],
        2 => [
          'count' => '532',
          'label' => 'introduced species',
        ],

      ],

    ],
    'islands' => [
      'data' => [
        0 => [
          'count' => '2.365',
          'label' => 'Aruba',
        ],

        1 => [
          'count' => '2.665',
          'label' => '(Klein) Bonaire',
        ],

        2 => [
          'count' => '4.949',
          'label' => '(Klein) Curaçao',
        ],

        3 => [
          'count' => '2.108',
          'label' => 'Saba',
        ],

        4 => [
          'count' => '839',
          'label' => 'Saba Bank',
        ],

        5 => [
          'count' => '2.869',
          'label' => 'St. Eustatius',
        ],

        6 => [
          'count' => '2.346',
          'label' => 'St. Maarten',
        ],

      ],

      'icon' => 'verspreidingskaarten.png',
      'label' => 'Species per island',
    ],

    'label' => 'Species occurring in Dutch Caribbean',
    'mainCount' => '9.337',
    'statistics' => [
      'dutch_names' => [
        'count' => '880',
        'icon' => 'nederlandsenamen.png',
        'label' => 'Dutch names',
      ],
      'english_names' => [
        'count' => '5.628',
        'icon' => 'nederlandsenamen.png',
        'label' => 'English names',
      ],

      'literature' => [
        'count' => '1.395',
        'icon' => 'literatuurbronnen.png',
        'label' => 'Literature references',
      ],

      'papiamento_names' => [
        'count' => '2.575',
        'icon' => 'nederlandsenamen.png',
        'label' => 'Papiamento names',
      ],

      'photographers' => [
        'count' => '65',
        'icon' => 'specialisten.png',
        'label' => 'Photographers',
      ],

      'species_photo_count' => [
        'count' => '1.245',
        'icon' => 'soortenmetfotos.png',
        'label' => '(Sub)species with photos',
      ],

      'taxon_photo_count' => [
        'count' => '5.265',
        'icon' => 'fotos.png',
        'label' => 'Photos',
      ],

      'validators' => [
        'count' => '79',
        'icon' => 'specialisten.png',
        'label' => 'Identification specialists',
      ],

    ],

  ];

  $statistics = new StatisticsData($statisticsData);
  Drupal::state()->set(LinnaeusBlock::STATISTICS_DATA, $statistics);

  $lastImageData = [
    'url_recent_images' => '/linnaeus_ng/app/views/search/nsr_recent_pictures.php',
    'images' => [
      [
        'media_id' => '72883',
        'taxon_id' => '155627',
        'file_name' => '139260_bhk_0704kopiea.jpg',
        'scientific_name' => 'Coccinella septempunctata',
        'dutch_name' => 'Zevenstippelig lieveheersbeestje',
        'fotograaf' => 'Bas van Hulst-Kuiper',
        'date_created' => '3 november 2020',
        'validator' => 'Gerrian Tacoma',
        'lokatie' => 'Ledeboerpark, Enschede, Overijssel',
      ],
    ],
  ];
  $lastImage = new LastImagesData($lastImageData);
  Drupal::state()->set(LinnaeusBlock::LAST_IMAGES_DATA, $lastImage);
}

/**
 * Fix path alias langcode.
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
function nsr_linnaeus_update_9000(): void {
  $connection = Drupal::database();
  $pathAlias = $connection->update('path_alias');
  $pathAlias->fields([
    'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
  ]);
  $pathAlias->execute();
  $pathAliasRevision = $connection->update('path_alias_revision');
  $pathAliasRevision->fields([
    'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
  ]);
  $pathAliasRevision->execute();
}
