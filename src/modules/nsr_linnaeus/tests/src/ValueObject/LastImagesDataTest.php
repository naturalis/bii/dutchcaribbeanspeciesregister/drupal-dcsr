<?php

namespace Drupal\Tests\nsr_linnaeus\ValueObject;

use Drupal\nsr_linnaeus\ValueObject\Image;
use Drupal\nsr_linnaeus\ValueObject\LastImagesData;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class LastImagesDataTest extends TestCase {

  /**
   * @param string $message
   * @param array $data
   *
   * @dataProvider missingAndWrongDataProvider
   */
  public function testMissingAndWrongData(string $message, array $data): void {
    $this->expectExceptionObject(new InvalidArgumentException($message));
    new LastImagesData($data);
  }

  /**
   * @return array
   */
  public function missingAndWrongDataProvider(): array {
    return [
      [
        "Missing 'url_recent_images' in the last images data",
        [],
      ],
      [
        "The 'url_recent_images' is not a string in the last images data",
        [
          'url_recent_images' => FALSE,
        ],
      ],
      [
        "Missing 'images' key/data in last images data",
        [
          'url_recent_images' => '',
        ],
      ],
      [
        "Missing 'images' key/data in last images data",
        [
          'url_recent_images' => '',
          'images' => FALSE,
        ],
      ],
      [
        "Missing 'images' key/data in last images data",
        [
          'url_recent_images' => '',
          'images' => [],
        ],
      ],
      [
        "Invalid image data in last images data",
        [
          'url_recent_images' => '',
          'images' => [
            FALSE,
          ],
        ],
      ],
    ];
  }

  /**
   * Valid data test.
   */
  public function testValidData(): void {
    $data = [
      'url_recent_images' => 'https://example.com/recent_images.php',
      'images' => [
        (new ImageTest())->validData(),
      ],
    ];
    $lastImageData = new LastImagesData($data);

    self::assertSame('https://example.com/recent_images.php', $lastImageData->getRecentImagesUrl());
    $images = $lastImageData->getImages();
    self::assertArrayHasKey(7331, $images);
    self::assertInstanceOf(Image::class, $images[7331]);
  }

  /**
   * Valid data with multiple images test.
   */
  public function testValidDataWithMultipleImages(): void {
    $validImageData = (new ImageTest())->validData();
    $data = [
      'url_recent_images' => 'https://example2.com/recent_images.php',
      'images' => [
        $validImageData,
      ],
    ];

    $validImageData['media_id'] = '7400';
    $data['images'][] = $validImageData;

    $lastImageData = new LastImagesData($data);

    self::assertSame('https://example2.com/recent_images.php', $lastImageData->getRecentImagesUrl());
    $images = $lastImageData->getImages();
    self::assertArrayHasKey(7331, $images);
    self::assertInstanceOf(Image::class, $images[7331]);
    self::assertArrayHasKey(7400, $images);
    self::assertInstanceOf(Image::class, $images[7400]);
  }

  /**
   * Is not equal url test.
   */
  public function testIsNotEqualUrl(): void {
    $currentData = $otherData = [
      'url_recent_images' => 'https://example.com/recent_images.php',
      'images' => [
        (new ImageTest())->validData(),
      ],
    ];
    $current = new LastImagesData($currentData);

    $otherData['url_recent_images'] = 'https://example.com/other_recent_images.php';
    $other = new LastImagesData($otherData);

    self::assertFalse($current->isEqual($other));
  }

  /**
   * Is not equal images test.
   */
  public function testIsNotEqualImages(): void {
    $validImageData = (new ImageTest())->validData();
    $currentData = [
      'url_recent_images' => 'https://example2.com/recent_images.php',
      'images' => [
        $validImageData,
      ],
    ];
    $validImageData['media_id'] = '7400';
    $currentData['images'][] = $validImageData;

    $otherData = $currentData;
    $otherData['images'][0]['media_id'] = '7401';

    $current = new LastImagesData($currentData);
    $other = new LastImagesData($otherData);

    self::assertFalse($current->isEqual($other));
  }

  /**
   * Is equal test.
   */
  public function testIsEqual(): void {
    $validImageData = (new ImageTest())->validData();
    $data = [
      'url_recent_images' => 'https://example2.com/recent_images.php',
      'images' => [
        $validImageData,
      ],
    ];

    $validImageData['media_id'] = '7400';
    $data['images'][] = $validImageData;

    $current = new LastImagesData($data);
    $other = new LastImagesData($data);

    self::assertTrue($current->isEqual($other));
  }

}
