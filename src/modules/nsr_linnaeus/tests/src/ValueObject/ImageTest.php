<?php

namespace Drupal\Tests\nsr_linnaeus\ValueObject;

use Drupal\nsr_linnaeus\ValueObject\Image;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use function array_combine;
use function array_diff;
use function array_fill;
use function count;

final class ImageTest extends TestCase {

  private const EXPECTED_KEYS = [
    'taxon_id',
    'media_id',
    'file_name',
    'scientific_name',
    'dutch_name',
    'fotograaf',
    'date_created',
    'validator',
    'lokatie',
  ];

  private const INTEGER_KEYS = [
    'taxon_id',
    'media_id',
  ];

  private const NULLABLE_KEYS = [
    'dutch_name',
    'fotograaf',
    'validator',
    'lokatie',
  ];

  /**
   * @param string $message
   * @param array $data
   *
   * @dataProvider missingAndWrongDataProvider
   */
  public function testMissingAndWrongData(string $message, array $data): void {
    $this->expectExceptionObject(new InvalidArgumentException($message));
    new Image($data);
  }

  /**
   * @return array
   */
  public function missingAndWrongDataProvider(): array {
    $testCases = [];
    foreach (self::EXPECTED_KEYS as $key) {
      $data = $this->validData();
      unset($data[$key]);
      $testCases["$key missing"] = [
        "Missing '$key' in the image data",
        $data,
      ];
    }

    $stringOnlyKeys = array_diff(self::EXPECTED_KEYS, self::INTEGER_KEYS, self::NULLABLE_KEYS);
    foreach ($stringOnlyKeys as $key) {
      $data = $this->validData();
      $data[$key] = FALSE;
      $testCases["$key wrong"] = [
        "The '$key' is not a string in the image data",
        $data,
      ];
    }

    foreach (self::INTEGER_KEYS as $key) {
      $data = $this->validData();
      $data[$key] = FALSE;
      $testCases["$key wrong"] = [
        "The '$key' is not numeric in the image data",
        $data,
      ];
    }

    foreach (self::NULLABLE_KEYS as $key) {
      $data = $this->validData();
      $data[$key] = FALSE;
      $testCases["$key wrong"] = [
        "The '$key' is not a string or null in the image data",
        $data,
      ];
    }
    return $testCases;
  }

  /**
   * @return array
   */
  public function validData(): array {
    return (array) array_combine(
      self::EXPECTED_KEYS,
      ['1337', '7331'] + array_fill(
        1,
        count(self::EXPECTED_KEYS) - 1,
        'string-value'
      )
    );
  }

  /**
   * Valid data test.
   */
  public function testValidData(): void {
    $image = new Image($this->validData());

    $this->assertImageFields($image);
  }

  /**
   * @param \Drupal\nsr_linnaeus\ValueObject\Image $image
   */
  private function assertImageFields(Image $image): void {
    self::assertSame(1337, $image->getTaxonId());
    self::assertSame(
      '/linnaeus_ng/app/views/species/nsr_taxon.php?epi=1&id=1337',
      $image->getTaxonUrl()
    );
    self::assertSame(7331, $image->getId());
    self::assertSame('https://images.naturalis.nl/w800/string-value', $image->getUrl());
    self::assertSame('string-value', $image->getScientificName());
    self::assertSame('string-value', $image->getDutchName());
    self::assertSame('string-value', $image->getPhotographer());
    self::assertSame('string-value', $image->getDateCreated());
    self::assertSame('string-value', $image->getValidator());
    self::assertSame('string-value', $image->getLocation());
  }

  /**
   * Valid data test.
   */
  public function testValidIntegerData(): void {
    $data = $this->validData();
    foreach (self::INTEGER_KEYS as $key) {
      $data[$key] = (int) $data[$key];
    }
    $image = new Image($data);

    $this->assertImageFields($image);
  }

  /**
   * Valid nullable data test.
   */
  public function testValidNullableData(): void {
    $data = $this->validData();
    foreach (self::NULLABLE_KEYS as $key) {
      $data[$key] = NULL;
    }
    $image = new Image($data);
    self::assertSame(1337, $image->getTaxonId());
    self::assertSame(
      '/linnaeus_ng/app/views/species/nsr_taxon.php?epi=1&id=1337',
      $image->getTaxonUrl()
    );
    self::assertSame(7331, $image->getId());
    self::assertSame('https://images.naturalis.nl/w800/string-value', $image->getUrl());
    self::assertSame('string-value', $image->getScientificName());
    self::assertNull($image->getDutchName());
    self::assertNull($image->getPhotographer());
    self::assertSame('string-value', $image->getDateCreated());
    self::assertNull($image->getValidator());
    self::assertNull($image->getLocation());
  }

}
