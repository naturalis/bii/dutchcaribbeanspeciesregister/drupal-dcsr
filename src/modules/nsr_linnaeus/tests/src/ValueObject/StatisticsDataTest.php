<?php

namespace Drupal\Tests\nsr_linnaeus\ValueObject;

use Drupal\nsr_linnaeus\ValueObject\StatisticsData;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use function array_combine;
use function array_fill;
use function array_flip;
use function array_pop;
use function array_slice;
use function count;
use function implode;
use function substr;

final class StatisticsDataTest extends TestCase {

  private const EXPECTED_STATISTICS = [
    'dutch_names',
    'english_names',
    'papiamento_names',
    'literature',
    'photographers',
    'species_photo_count',
    'taxon_photo_count',
    'validators',
  ];
  private const STATISTIC_KEYS = [
    'count',
    'label',
    'icon',
  ];

  /**
   * @param string $message
   * @param array $data
   *
   * @dataProvider missingAndWrongDataProvider
   * @dataProvider missingStatisticsDataProvider
   * @dataProvider wrongStatisticsTypeDataProvider
   * @dataProvider missingStatisticsSubKeyDataProvider
   * @dataProvider wrongStatisticsSubKeyDataProvider
   */
  public function testMissingAndWrongData(string $message, array $data): void {
    $this->expectExceptionObject(new InvalidArgumentException($message));
    new StatisticsData($data);
  }

  /**
   * @return array[]
   */
  public function missingAndWrongDataProvider(): array {
    return [
      'Missing mainCount' => [
        "Missing 'mainCount' in the statistics data",
        [],
      ],
      'Wrong mainCount type' => [
        "The 'mainCount' is not a string in the statistics data",
        [
          'mainCount' => FALSE,
        ],
      ],
      'Wrong statistics type' => [
        "Missing 'statistics' key/data in statistics data",
        [
          'mainCount' => '10',
          'statistics' => 'Wrong',
        ],
      ],
      'Empty statistics' => [
        "Missing 'statistics' key/data in statistics data",
        [
          'mainCount' => '10',
          'statistics' => [],
        ],
      ],
      'No valid statistics' => [
        "Missing 'statistics' sub keys in the statistics data: " . implode(', ', self::EXPECTED_STATISTICS),
        [
          'mainCount' => '10',
          'statistics' => [
            'la' => '',
          ],
        ],
      ],
    ];
  }

  /**
   * @return array[]
   */
  public function missingStatisticsDataProvider(): array {
    $testCases = [];
    $statistics = self::EXPECTED_STATISTICS;
    // Remove last entry as that will throw a different error.
    array_pop($statistics);
    foreach ($statistics as $key => $statistic) {
      $amount = $key + 1;
      $missingKeys = implode(', ', array_slice(self::EXPECTED_STATISTICS, $amount));
      $testCases[$statistic] = [
        'Missing statistics sub keys in the statistics data: ' . $missingKeys,
        [
          'mainCount' => '10',
          'statistics' => array_flip(
            array_slice(self::EXPECTED_STATISTICS, 0, $amount)
          ),
        ],
      ];
    }
    return $testCases;
  }

  /**
   * @return array[]
   */
  public function wrongStatisticsTypeDataProvider(): array {
    $testCases = [];
    foreach (self::EXPECTED_STATISTICS as $statistic) {
      $providedData = $this->validData();
      $providedData['statistics'][$statistic] = '';
      $testCases[$statistic] = [
        "The sub key '$statistic' is not an array in the statistics data",
        $providedData,
      ];
    }
    return $testCases;
  }

  /**
   * @return array[]
   */
  public function missingStatisticsSubKeyDataProvider(): array {
    $testCases = [];
    foreach (self::EXPECTED_STATISTICS as $statistic) {
      foreach (self::STATISTIC_KEYS as $key) {
        $testData = $this->validData();
        unset($testData['statistics'][$statistic][$key]);
        $testCases["$statistic [$key]"] = [
          "Missing sub key '$key' from statistics '$statistic' in the statistics data",
          $testData,
        ];
      }
    }
    return $testCases;
  }

  /**
   * @return array
   */
  private function validData(): array {
    return [
      'mainCount' => '1.000',
      'islands' => [
        'data' => [],
      ],
      'categories' => [
        'data' => [],
      ],
      'statistics' => array_combine(
        self::EXPECTED_STATISTICS,
        array_fill(
          0,
          count(self::EXPECTED_STATISTICS),
          [
            'count' => '1.234',
            'label' => 'Test label',
            'icon' => 'someicon.png',
          ]
        )
      ),
    ];
  }

  /**
   * @return array[]
   */
  public function wrongStatisticsSubKeyDataProvider(): array {
    $testCases = [];
    foreach (self::EXPECTED_STATISTICS as $statistic) {
      foreach (self::STATISTIC_KEYS as $key) {
        $testData = $this->validData();
        $testData['statistics'][$statistic][$key] = FALSE;
        $testCases["$statistic [$key]"] = [
          "The sub key '$key' from statistics '$statistic' is not a string in the statistics data",
          $testData,
        ];
      }
    }
    return $testCases;
  }

  /**
   * Valid data test.
   */
  public function testValidData(): void {
    $validData = $this->validData();
    $data = new StatisticsData($validData);
    self::assertSame('1.000', $data->getMainCount());

    $expectedStatistics = $validData['statistics'];
    foreach ($expectedStatistics as &$statistic) {
      $icon = $statistic['icon'];
      $statistic['image'] = "/modules/custom/nsr_linnaeus/images/$icon";
    }
    unset($statistic);
    self::assertSame($expectedStatistics, $data->getStatistics());
  }

  /**
   * Ignored datat test.
   */
  public function testIgnoredData(): void {
    $expectedData = $paddedData = $this->validData();
    $paddedData['other'] = 'value';
    $paddedData['statistics']['other_statistic1'] = [
      'count' => '123',
      'label' => 'Does not really matter',
    ];
    $paddedData['statistics']['other_statistic2'] = 'does not matter';
    $expected = new StatisticsData($expectedData);
    $padded = new StatisticsData($paddedData);

    self::assertSame($expected->getMainCount(), $padded->getMainCount());
    self::assertSame($expected->getStatistics(), $padded->getStatistics());
    self::assertTrue($expected->isEqual($padded));
  }

  /**
   * @param \Drupal\nsr_linnaeus\ValueObject\StatisticsData $other
   * @param bool $expected
   *
   * @dataProvider isEqualDataProvider
   * @dataProvider differentStatisticsDataProvider
   */
  public function testIsEqual(StatisticsData $other, bool $expected): void {
    $data = new StatisticsData($this->validData());
    self::assertSame($expected, $data->isEqual($other));
  }

  /**
   * @return array[]
   */
  public function isEqualDataProvider(): array {
    return [
      [
        new StatisticsData(
          [
            'mainCount' => '1001',
          ] + $this->validData()
        ),
        FALSE,
      ],
      [
        new StatisticsData(
          [
            'some_other' => 'irrelevant value',
          ] + $this->validData()
        ),
        TRUE,
      ],
    ];
  }

  /**
   * @return array[]
   */
  public function differentStatisticsDataProvider(): array {
    $data = [];
    $validData = $this->validData();
    foreach (self::EXPECTED_STATISTICS as $statistic) {
      foreach (self::STATISTIC_KEYS as $key) {
        $changedShorter = [
          $statistic => [
            $key => substr($validData['statistics'][$statistic][$key], 0, -1),
          ],
        ];
        $data[] = [
          new StatisticsData(
            $this->changeStatistics(
              $validData,
              $changedShorter
            ),
          ),
          FALSE,
        ];

        $changedLonger = [
          $statistic => [
            $key => $validData['statistics'][$statistic][$key] . '1',
          ],
        ];
        $data[] = [
          new StatisticsData(
            $this->changeStatistics(
              $validData,
              $changedLonger
            ),
          ),
          FALSE,
        ];
      }
    }
    return $data;
  }

  /**
   * @param array $originalData
   * @param array $changedStatistics
   *
   * @return array[]
   */
  private function changeStatistics(array $originalData, array $changedStatistics): array {
    foreach ($changedStatistics as $statistic => $row) {
      foreach ($row as $key => $value) {
        $originalData['statistics'][$statistic][$key] = $value;
      }
    }
    return $originalData;
  }

}
