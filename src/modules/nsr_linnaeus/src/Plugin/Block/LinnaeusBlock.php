<?php

namespace Drupal\nsr_linnaeus\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\file\FileStorageInterface;
use Drupal\nsr_linnaeus\ValueObject\StatisticsData;
use InvalidArgumentException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function implode;

/**
 * @Block(
 *  id = "linnaeus_block",
 *  admin_label = @Translation("Linnaeus block"),
 * )
 *
 * Ignore false positives on $form without array as the interface forces this.
 * phpcs:disable Drupal.Commenting.FunctionComment.TypeHintMissing
 *
 * Ignore false positives on not used paramater as the interface forces this.
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class LinnaeusBlock extends BlockBase implements ContainerFactoryPluginInterface {

  public const STATISTICS_URI = self:: PREFIX . self::STATISTICS_URL;
  public const STATISTICS_DATA = self:: PREFIX . 'statistics_data';
  public const STATISTICS_TAG = self::PREFIX . self::STATISTICS;

  public const LAST_IMAGES_URI = self::PREFIX . self::LAST_IMAGES_URL;
  public const LAST_IMAGES_DATA = self::PREFIX . 'last_images_data';
  public const LAST_IMAGES_TAG = self::PREFIX . self::LAST_IMAGES;

  private const PREFIX = 'linnaeus_block_';
  private const STATISTICS = 'statistics';
  private const STATISTICS_URL = 'statistics_url';
  private const LAST_IMAGES = 'last_images';
  private const LAST_IMAGES_URL = 'last_images_url';
  private const IMAGE_SEARCH_PAGE_URL = 'image_search_page_url';
  private const URL_PATTERN = '^(https?:\/\/([\w-]+\.)+[a-z]{2,})?(\/[^#?\s\/]+)*(\/)?(\?[^\s#]+)?(#[^\s]*)?$';


  /**
   * @var \Drupal\Core\State\StateInterface
   */
  private StateInterface $state;

  /**
   * @var \Drupal\file\FileStorageInterface
   * @phpstan-ignore-next-line
   */
  private FileStorageInterface $fileStorage;

  /**
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   * @phpstan-ignore-next-line
   */
  private ConfigEntityStorageInterface $imageStyleStorage;

  /**
   * @param array $configuration
   * @param string $pluginId
   * @param array $pluginDefinition
   * @param \Drupal\Core\State\StateInterface $state
   * @param \Drupal\file\FileStorageInterface $fileStorage
   * @param \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $imageStyleStorage
   */
  public function __construct(
    array $configuration,
    string $pluginId,
    array $pluginDefinition,
    StateInterface $state,
    FileStorageInterface $fileStorage,
    ConfigEntityStorageInterface $imageStyleStorage,
  ) {
    parent::__construct(
      $configuration,
      $pluginId,
      $pluginDefinition
    );
    $this->state = $state;
    $this->fileStorage = $fileStorage;
    $this->imageStyleStorage = $imageStyleStorage;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $pluginId
   * @param array $pluginDefinition
   *
   * @return \Drupal\nsr_linnaeus\Plugin\Block\LinnaeusBlock
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition,
  ) {
    return new self(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('state'),
      $container->get('nsr_linnaeus.file.storage'),
      $container->get('nsr_linnaeus.image_style.storage')
    );
  }

  /**
   * @return array
   */
  public function defaultConfiguration(): array {
    return [
      self::STATISTICS_URL => '',
      self::LAST_IMAGES_URL => '',
      self::IMAGE_SEARCH_PAGE_URL => '',
    ];
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *
   * @return array
   */
  public function blockForm($form, FormStateInterface $formState): array {
    $form[self::STATISTICS] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Statistics'),
    ];
    $form[self::STATISTICS][self::STATISTICS_URL] = [
      '#type' => 'textfield',
      '#title' => $this->t('Statistics URL'),
      '#default_value' => $this->configuration[self::STATISTICS_URL],
      '#pattern' => self::URL_PATTERN,
    ];

    $form[self::LAST_IMAGES] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Last added images'),
    ];
    $form[self::LAST_IMAGES][self::LAST_IMAGES_URL] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last images API URL'),
      '#default_value' => $this->configuration[self::LAST_IMAGES_URL],
      '#pattern' => self::URL_PATTERN,
    ];
    $form[self::LAST_IMAGES][self::IMAGE_SEARCH_PAGE_URL] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image search page URL'),
      '#default_value' => $this->configuration[self::IMAGE_SEARCH_PAGE_URL],
      '#pattern' => self::URL_PATTERN,
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $formState
   */
  public function blockValidate($form, FormStateInterface $formState): void {
    $this->validateUri($formState, [self::STATISTICS, self::STATISTICS_URL]);
    $this->validateUri($formState, [self::LAST_IMAGES, self::LAST_IMAGES_URL]);
    $this->validateUri($formState, [
      self::LAST_IMAGES,
      self::IMAGE_SEARCH_PAGE_URL,
    ]);
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @param array $selector
   */
  private function validateUri(FormStateInterface $formState, array $selector): void {
    $uri = $formState->getValue($selector);
    if ($uri === '') {
      return;
    }

    try {
      /* @phpstan-ignore-next-line */
      $this->createUrl($uri);
    }
    catch (InvalidArgumentException $exception) {
      $formState->setErrorByName(implode('][', $selector), $exception->getMessage());
    }
  }

  /**
   * @param string $uri
   *
   * @return \Drupal\Core\Url
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  private function createUrl(string $uri): Url {
    if ($uri[0] === '/') {
      $uri = 'internal:' . $uri;
    }
    return Url::fromUri($uri);
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $formState
   */
  public function blockSubmit($form, FormStateInterface $formState): void {
    $statisticsUrl = $formState->getValue(
      [self::STATISTICS, self::STATISTICS_URL]
    );
    $this->configuration[self::STATISTICS_URL] = $statisticsUrl;
    /* @phpstan-ignore-next-line */
    $this->state->set(self::STATISTICS_URI, $this->getAbsoluteUrl($statisticsUrl));

    $imageApiUrl = $formState->getValue(
      [self::LAST_IMAGES, self::LAST_IMAGES_URL]
    );
    $this->configuration[self::LAST_IMAGES_URL] = $imageApiUrl;
    /* @phpstan-ignore-next-line */
    $this->state->set(self::LAST_IMAGES_URI, $this->getAbsoluteUrl($imageApiUrl));
    $this->configuration[self::IMAGE_SEARCH_PAGE_URL] = $formState->getValue(
      [self::LAST_IMAGES, self::IMAGE_SEARCH_PAGE_URL]
    );

  }

  /**
   * @param string $uri
   *
   * @return string
   */
  private function getAbsoluteUrl(string $uri): string {
    if ($uri === '') {
      return '';
    }

    $url = $this->createUrl($uri);
    $url->setAbsolute();
    /** @var string $absoluteUrl */
    $absoluteUrl = $url->toString();
    return $absoluteUrl;
  }

  /**
   * @return array
   */
  public function build(): array {
    return [
      '#attributes' => [
        'class' => ['linnaeus-block'],
      ],
      'statistics' => $this->getStatisticsBlock(),
      'column' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['column'],
        ],
        'last_image' => $this->getLastImageBlock(),
      ],
    ];
  }

  /**
   * @return array
   */
  private function getStatisticsBlock(): array {
    $statisticsData = $this->state->get(self::STATISTICS_DATA);
    if (!$statisticsData instanceof StatisticsData) {
      return [
        '#cache' => [
          'tags' => [
            self::STATISTICS_TAG,
          ],
        ],
      ];
    }

    return [
      '#theme' => 'nsr_linnaeus_statistics_block',
      '#mainCount' => $statisticsData->getMainCount(),
      '#categories' => $statisticsData->getCategories(),
      '#islands' => $statisticsData->getIslands(),
      '#statistics' => $statisticsData->getStatistics(),
      '#cache' => [
        'tags' => [
          self::STATISTICS_TAG,
        ],
      ],
    ];
  }

  /**
   * @return array
   */
  private function getLastImageBlock(): array {
    return [
      '#lazy_builder' => [
        'nsr_linnaeus.last_image_block.lazy_builder:render',
        [$this->configuration[self::IMAGE_SEARCH_PAGE_URL]],
      ],
      '#create_placeholder' => TRUE,
    ];
  }

}
