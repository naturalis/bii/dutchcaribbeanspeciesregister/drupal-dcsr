<?php

namespace Drupal\nsr_linnaeus\ValueObject;

use InvalidArgumentException;
use function array_diff_assoc;
use function array_diff_key;
use function array_keys;
use function implode;
use function is_array;
use function is_string;
use function str_replace;

final class StatisticsData {

  private const STATISTICS_KEYS = [
    'dutch_names' => '',
    'english_names' => '',
    'papiamento_names' => '',
    'literature' => '',
    'photographers' => '',
    'species_photo_count' => '',
    'taxon_photo_count' => '',
    'validators' => '',
  ];

  /**
   * @var string
   */
  private string $mainCount;

  /**
   * @var string[][]
   */
  private array $categories;

  /**
   * @var string[][]
   */
  private array $islands;

  /**
   * @var string[][]
   */
  private array $statistics;

  /**
   * @param array $data
   */
  public function __construct(array $data) {
    $this->mainCount = $this->getString('mainCount', $data);

    if (empty($data['statistics']) || !is_array($data['statistics'])) {
      throw new InvalidArgumentException("Missing 'statistics' key/data in statistics data");
    }
    $this->setStatistics($data['statistics']);

    if (empty($data['islands']) || !is_array($data['islands'])) {
      throw new InvalidArgumentException("Missing 'islands' key/data in statistics data");
    }
    $this->setIslands($data['islands']);

    if (empty($data['categories']) || !is_array($data['categories'])) {
      throw new InvalidArgumentException("Missing 'categories' key/data in statistics data");
    }
    $this->setCategories($data['categories']);
  }

  /**
   * @param string $key
   * @param array $data
   *
   * @return string
   *
   * @throws \InvalidArgumentException
   */
  private function getString(string $key, array $data): string {
    if (!isset($data[$key])) {
      throw new InvalidArgumentException("Missing '$key' in the statistics data");
    }
    if (!is_string($data[$key])) {
      throw new InvalidArgumentException("The '$key' is not a string in the statistics data");
    }
    return $data[$key];
  }

  /**
   * @param string[][] $statistics
   *
   * @throws \InvalidArgumentException
   */
  private function setStatistics(array $statistics): void {
    $diff = array_diff_key(self::STATISTICS_KEYS, $statistics);
    if ($diff !== []) {
      $subKeys = implode(', ', array_keys($diff));
      throw new InvalidArgumentException("Missing 'statistics' sub keys in the statistics data: $subKeys");
    }

    foreach (array_keys(self::STATISTICS_KEYS) as $key) {
      if (!is_array($statistics[$key])) {
        throw new InvalidArgumentException("The sub key '$key' is not an array in the statistics data");
      }
      try {
        $icon = $this->getString('icon', $statistics[$key]);
        $this->statistics[$key] = [
          'count' => $this->getString('count', $statistics[$key]),
          'label' => $this->getString('label', $statistics[$key]),
          'icon' => $icon,
          'image' => "/modules/custom/nsr_linnaeus/images/$icon",
        ];
      }
      catch (InvalidArgumentException $exception) {
        $message = $exception->getMessage();
        $message = str_replace(
          [
            "'count'",
            "'label'",
            "'icon'",
          ],
          [
            "sub key 'count' from statistics '$key'",
            "sub key 'label' from statistics '$key'",
            "sub key 'icon' from statistics '$key'",
          ],
          $message
        );
        throw new InvalidArgumentException($message);
      }
    }
  }

  /**
   * @param string[][] $islands
   *
   * @throws \InvalidArgumentException
   */
  private function setIslands(array $islands): void {
    if (!is_array($islands['data'])) {
      throw new InvalidArgumentException("The sub key 'data' is not an array in the islands data");
    }
    foreach ($islands['data'] as $island) {
      $this->islands[] = [
        'count' => $this->getString('count', (array) $island),
        'label' => $this->getString('label', (array) $island),
      ];
    }

  }

  /**
   * @param string[][] $categories
   *
   * @throws \InvalidArgumentException
   */
  private function setCategories(array $categories): void {
    if (!is_array($categories['data'])) {
      throw new InvalidArgumentException("The sub key 'data' is not an array in the categories data");
    }
    foreach ($categories['data'] as $category) {
      $this->categories[] = [
        'count' => $this->getString('count', (array) $category),
        'label' => $this->getString('label', (array) $category),
      ];
    }
  }

  /**
   * @return string
   */
  public function getMainCount(): string {
    return $this->mainCount;
  }

  /**
   * @return string[][]
   */
  public function getStatistics(): array {
    return $this->statistics;
  }

  /**
   * @return string[][]
   */
  public function getIslands(): array {
    return $this->islands;
  }

  /**
   * @return string[][]
   */
  public function getCategories(): array {
    return $this->categories;
  }

  /**
   * @param \Drupal\nsr_linnaeus\ValueObject\StatisticsData $other
   *
   * @return bool
   */
  public function isEqual(StatisticsData $other): bool {
    if ($this->mainCount !== $other->getMainCount()) {
      return FALSE;
    }
    $otherStatistics = $other->getStatistics();
    foreach ($this->statistics as $key => $statistic) {
      if (array_diff_assoc($statistic, $otherStatistics[$key])) {
        return FALSE;
      }
    }
    return TRUE;
  }

}
