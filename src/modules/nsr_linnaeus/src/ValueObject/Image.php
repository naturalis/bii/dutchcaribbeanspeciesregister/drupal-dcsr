<?php

namespace Drupal\nsr_linnaeus\ValueObject;

use InvalidArgumentException;
use function array_key_exists;
use function is_numeric;
use function is_string;

final class Image {

  /**
   * @var int
   */
  private int $taxonId;

  /**
   * @var string
   */
  private string $taxonUrl;

  /**
   * @var int
   */
  private int $imageId;

  /**
   * @var string
   */
  private string $url;

  /**
   * @var string
   */
  private string $scientificName;

  /**
   * @var string|null
   */
  private ?string $dutchName;

  /**
   * @var string|null
   */
  private ?string $photographer;

  /**
   * @var string
   */
  private string $dateCreated;

  /**
   * @var string|null
   */
  private ?string $validator;

  /**
   * @var string|null
   */
  private ?string $location;

  /**
   * @param array $data
   */
  public function __construct(array $data) {
    $this->imageId = $this->getInteger('media_id', $data);
    $this->taxonId = $this->getInteger('taxon_id', $data);
    $this->taxonUrl = "/linnaeus_ng/app/views/species/nsr_taxon.php?epi=1&id=$this->taxonId";
    $this->url = 'https://images.naturalis.nl/w800/' . $this->getString('file_name', $data);
    $this->scientificName = $this->getString('scientific_name', $data);
    $this->dutchName = $this->getStringOrNull('dutch_name', $data);
    $this->photographer = $this->getStringOrNull('fotograaf', $data);
    $this->dateCreated = $this->getString('date_created', $data);
    $this->validator = $this->getStringOrNull('validator', $data);
    $this->location = $this->getStringOrNull('lokatie', $data);
  }

  /**
   * @param string $key
   * @param array $data
   *
   * @return int
   */
  private function getInteger(string $key, array $data): int {
    if (!isset($data[$key])) {
      throw new InvalidArgumentException("Missing '$key' in the image data");
    }
    if (!is_numeric($data[$key])) {
      throw new InvalidArgumentException("The '$key' is not numeric in the image data");
    }
    return (int) $data[$key];
  }

  /**
   * @param string $key
   * @param array $data
   *
   * @return string
   */
  private function getString(string $key, array $data): string {
    if (!isset($data[$key])) {
      throw new InvalidArgumentException("Missing '$key' in the image data");
    }
    if (!is_string($data[$key])) {
      throw new InvalidArgumentException("The '$key' is not a string in the image data");
    }
    return $data[$key];
  }

  /**
   * @param string $key
   * @param array $data
   *
   * @return string|null
   */
  private function getStringOrNull(string $key, array $data): ?string {
    if (!array_key_exists($key, $data)) {
      throw new InvalidArgumentException("Missing '$key' in the image data");
    }
    if (!is_string($data[$key]) && $data[$key] !== NULL) {
      throw new InvalidArgumentException("The '$key' is not a string or null in the image data");
    }
    return $data[$key];
  }

  /**
   * @return int
   */
  public function getTaxonId(): int {
    return $this->taxonId;
  }

  /**
   * @return string
   */
  public function getTaxonUrl(): string {
    return $this->taxonUrl;
  }

  /**
   * @return int
   */
  public function getId(): int {
    return $this->imageId;
  }

  /**
   * @return string
   */
  public function getUrl(): string {
    return $this->url;
  }

  /**
   * @return string
   */
  public function getScientificName(): string {
    return $this->scientificName;
  }

  /**
   * @return string|null
   */
  public function getDutchName(): ?string {
    return $this->dutchName;
  }

  /**
   * @return string|null
   */
  public function getPhotographer(): ?string {
    return $this->photographer;
  }

  /**
   * @return string
   */
  public function getDateCreated(): string {
    return $this->dateCreated;
  }

  /**
   * @return string|null
   */
  public function getValidator(): ?string {
    return $this->validator;
  }

  /**
   * @return string|null
   */
  public function getLocation(): ?string {
    return $this->location;
  }

}
