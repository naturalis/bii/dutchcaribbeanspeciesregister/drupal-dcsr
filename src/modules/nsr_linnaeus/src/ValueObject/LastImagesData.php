<?php

namespace Drupal\nsr_linnaeus\ValueObject;

use InvalidArgumentException;
use function array_diff_key;
use function is_array;
use function is_string;

final class LastImagesData {

  /**
   * @var string
   */
  private string $recentImagesUrl;

  /**
   * @var \Drupal\nsr_linnaeus\ValueObject\Image[]
   */
  private array $images = [];

  /**
   * @param array $data
   */
  public function __construct(array $data) {
    $this->recentImagesUrl = $this->getString('url_recent_images', $data);

    if (empty($data['images']) || !is_array($data['images'])) {
      throw new InvalidArgumentException("Missing 'images' key/data in last images data");
    }
    $this->setImages($data['images']);
  }

  /**
   * @param string $key
   * @param array $data
   *
   * @return string
   */
  private function getString(string $key, array $data): string {
    if (!isset($data[$key])) {
      throw new InvalidArgumentException("Missing '$key' in the last images data");
    }
    if (!is_string($data[$key])) {
      throw new InvalidArgumentException("The '$key' is not a string in the last images data");
    }
    return $data[$key];
  }

  /**
   * @param array $images
   */
  private function setImages(array $images): void {
    foreach ($images as $data) {
      if (!is_array($data)) {
        throw new InvalidArgumentException("Invalid image data in last images data");
      }
      $image = new Image($data);
      $this->images[$image->getId()] = $image;
    }
  }

  /**
   * @return string
   */
  public function getRecentImagesUrl(): string {
    return $this->recentImagesUrl;
  }

  /**
   * @return \Drupal\nsr_linnaeus\ValueObject\Image[]
   */
  public function getImages(): array {
    return $this->images;
  }

  /**
   * @param \Drupal\nsr_linnaeus\ValueObject\LastImagesData $other
   *
   * @return bool
   */
  public function isEqual(LastImagesData $other): bool {
    if ($this->recentImagesUrl !== $other->getRecentImagesUrl()) {
      return FALSE;
    }
    return array_diff_key($this->images, $other->getImages()) === [];
  }

}
