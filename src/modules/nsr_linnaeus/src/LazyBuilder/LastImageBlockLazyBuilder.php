<?php

namespace Drupal\nsr_linnaeus\LazyBuilder;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\State\StateInterface;
use Drupal\nsr_linnaeus\Plugin\Block\LinnaeusBlock;
use Drupal\nsr_linnaeus\ValueObject\LastImagesData;
use function array_rand;

final class LastImageBlockLazyBuilder implements TrustedCallbackInterface {

  use DependencySerializationTrait;

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  private StateInterface $state;

  /**
   * @param \Drupal\Core\State\StateInterface $state
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * @param string $searchPageUrl
   *
   * @return array
   */
  public function render(string $searchPageUrl): array {
    $lastImagesData = $this->state->get(LinnaeusBlock::LAST_IMAGES_DATA);
    if (!$lastImagesData instanceof LastImagesData) {
      return [
        '#cache' => [
          'tags' => [
            LinnaeusBlock::LAST_IMAGES_TAG,
          ],
        ],
      ];
    }

    $images = $lastImagesData->getImages();
    $image = $images[array_rand($images)];
    return [
      '#theme' => 'nsr_linnaeus_last_image_block',
      '#taxonUrl' => $image->getTaxonUrl(),
      '#imageUrl' => $image->getUrl(),
      '#scientificName' => $image->getScientificName(),
      '#dutchName' => $image->getDutchName(),
      '#photographer' => $image->getPhotographer(),
      '#dateCreated' => $image->getDateCreated(),
      '#validator' => $image->getValidator(),
      '#location' => $image->getLocation(),
      '#recentImagesUrl' => $lastImagesData->getRecentImagesUrl(),
      '#searchPageUrl' => $searchPageUrl,
      '#cache' => [
        'tags' => [
          LinnaeusBlock::LAST_IMAGES_TAG,
        ],
        'max-age' => 60,
      ],
    ];
  }

  /**
   * @return string[]
   */
  public static function trustedCallbacks(): array {
    return [
      'render',
    ];
  }

}
