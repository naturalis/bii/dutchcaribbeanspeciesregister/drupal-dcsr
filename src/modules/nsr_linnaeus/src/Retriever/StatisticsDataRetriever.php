<?php

namespace Drupal\nsr_linnaeus\Retriever;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\State\StateInterface;
use Drupal\nsr_linnaeus\Plugin\Block\LinnaeusBlock;
use Drupal\nsr_linnaeus\ValueObject\StatisticsData;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use function json_decode;

final class StatisticsDataRetriever {

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  private StateInterface $state;

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  private ClientInterface $client;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  private CacheTagsInvalidatorInterface $invalidator;

  /**
   * @param \Drupal\Core\State\StateInterface $state
   * @param \GuzzleHttp\ClientInterface $client
   * @param \Psr\Log\LoggerInterface $logger
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $invalidator
   */
  public function __construct(
    StateInterface $state,
    ClientInterface $client,
    LoggerInterface $logger,
    CacheTagsInvalidatorInterface $invalidator,
  ) {
    $this->state = $state;
    $this->client = $client;
    $this->logger = $logger;
    $this->invalidator = $invalidator;
  }

  /**
   * Update data.
   */
  public function update(): void {
    $uri = $this->state->get(LinnaeusBlock::STATISTICS_URI, '');
    if ($uri === '') {
      $this->state->delete(LinnaeusBlock::STATISTICS_DATA);
      $this->logger->info('Linnaeus statistics block has no URL configured, deleted state data');
      return;
    }

    try {
      /* @phpstan-ignore-next-line */
      $response = $this->client->request('GET', $uri);
    }
    catch (GuzzleException $exception) {
      $this->logger->error('Failed to retrieve new statistics block data: ' . $exception->getMessage());
      return;
    }

    $body = (string) $response->getBody();
    $data = json_decode($body, TRUE);
    if ($data === NULL) {
      $this->logger->error('Statistics API returned faulty json');
      return;
    }

    try {
      /* @phpstan-ignore-next-line */
      $newData = new StatisticsData($data);
    }
    catch (InvalidArgumentException $exception) {
      $this->logger->error('Statistics API returned faulty data: ' . $exception->getMessage());
      return;
    }

    $currentData = $this->state->get(LinnaeusBlock::STATISTICS_DATA);
    if ($currentData instanceof StatisticsData && $currentData->isEqual($newData)) {
      $this->logger->info('Statistics API returned same data as currently saved in state');
      return;
    }

    $this->state->set(LinnaeusBlock::STATISTICS_DATA, $newData);
    $this->invalidator->invalidateTags([LinnaeusBlock::STATISTICS_TAG]);
    $this->logger->info('Statistics API returned different data: updated state and invalidated tag');
  }

}
