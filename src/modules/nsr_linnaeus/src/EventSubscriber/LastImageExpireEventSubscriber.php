<?php

namespace Drupal\nsr_linnaeus\EventSubscriber;

use DateTime;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\nsr_linnaeus\Plugin\Block\LinnaeusBlock;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use function in_array;

final class LastImageExpireEventSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private TimeInterface $time;

  /**
   * @param \Drupal\Component\Datetime\TimeInterface $time
   */
  public function __construct(TimeInterface $time) {
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::RESPONSE => 'onResponse',
    ];
  }

  /**
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *
   * @throws \Exception
   */
  public function onResponse(ResponseEvent $event): void {
    if (!$event->isMainRequest()) {
      return;
    }

    $response = $event->getResponse();
    if (!$response instanceof CacheableResponseInterface || !$response->isCacheable()) {
      return;
    }

    $metadata = $response->getCacheableMetadata();
    if (!in_array(LinnaeusBlock::LAST_IMAGES_TAG, $metadata->getCacheTags(), TRUE)) {
      return;
    }

    $expires = new DateTime('@' . ($this->time->getRequestTime() + $metadata->getCacheMaxAge()));
    $response->setExpires($expires);
  }

}
