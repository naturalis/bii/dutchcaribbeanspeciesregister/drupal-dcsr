<?php

namespace Drupal\nsr_slideshow\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node\NodeInterface;
use Drupal\node\NodeStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function array_rand;

/**
 * @Block(
 *   id = "slideshow_block",
 *   admin_label = @Translation("Slideshow block"),
 * )
 */
class SlideshowBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\node\NodeStorageInterface
   */
  private NodeStorageInterface $nodeStorage;

  /**
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  private EntityViewBuilderInterface $nodeViewBuilder;

  /**
   * @param array $configuration
   * @param string $pluginId
   * @param array $definition
   * @param \Drupal\node\NodeStorageInterface $nodeStorage
   * @param \Drupal\Core\Entity\EntityViewBuilderInterface $nodeViewBuilder
   */
  public function __construct(
    array $configuration,
    string $pluginId,
    array $definition,
    NodeStorageInterface $nodeStorage,
    EntityViewBuilderInterface $nodeViewBuilder,
  ) {
    parent::__construct($configuration, $pluginId, $definition);
    $this->nodeStorage = $nodeStorage;
    $this->nodeViewBuilder = $nodeViewBuilder;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $pluginId
   * @param array $definition
   *
   * @return \Drupal\nsr_slideshow\Plugin\Block\SlideshowBlock
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * Ignore false positive on missing array parameter declaration.
   * phpcs:disable Drupal.Commenting.FunctionComment.TypeHintMissing
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $definition) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = $container->get('entity_type.manager');
    /** @var \Drupal\node\NodeStorageInterface $nodeStorage */
    $nodeStorage = $entityTypeManager->getStorage('node');
    return new self(
      $configuration,
      $pluginId,
      $definition,
      $nodeStorage,
      $entityTypeManager->getViewBuilder('node')
    );
  }

  /**
   * @return string[]|array[]
   */
  public function build(): array {
    $slideshow = $this->getRandomSlideshow();
    if (!$slideshow instanceof NodeInterface) {
      return [
        '#markup' => '',
        '#cache' => [
          'tags' => [
            'node_list:dia',
          ],
          'max-age' => 3600,
        ],
      ];
    }

    $build = $this->nodeViewBuilder->view($slideshow);
    $build['#cache']['contexts'][] = 'url.path';
    $build['#cache']['max-age'] = 3600;
    return $build;
  }

  /**
   * @return \Drupal\node\NodeInterface|null
   */
  private function getRandomSlideshow(): ?NodeInterface {
    $query = $this->nodeStorage->getQuery();
    $query->condition('status', NodeInterface::PUBLISHED);
    $query->condition('type', 'dia');

    /** @var int[] $slideshowIds */
    $slideshowIds = $query->accessCheck(FALSE)->execute();
    if ($slideshowIds === []) {
      return NULL;
    }

    $slideshowId = $slideshowIds[array_rand($slideshowIds)];
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->nodeStorage->load($slideshowId);
    return $node;
  }

}
