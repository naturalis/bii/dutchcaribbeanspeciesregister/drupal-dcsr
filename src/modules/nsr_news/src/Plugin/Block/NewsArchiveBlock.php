<?php

namespace Drupal\nsr_news\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use PDO;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *   id = "news_archive_block",
 *   admin_label = @Translation("News archive block"),
 * )
 *
 * Ignore false positive on missing array parameter declaration.
 * phpcs:disable Drupal.Commenting.FunctionComment.TypeHintMissing
 */
class NewsArchiveBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Database\Connection
   */
  private Connection $database;

  /**
   * @param array $configuration
   * @param string $pluginId
   * @param array $definition
   * @param \Drupal\Core\Database\Connection $database
   */
  public function __construct(
    array $configuration,
    string $pluginId,
    array $definition,
    Connection $database,
  ) {
    parent::__construct($configuration, $pluginId, $definition);
    $this->database = $database;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $pluginId
   * @param array $definition
   *
   * @return \Drupal\nsr_news\Plugin\Block\NewsArchiveBlock
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $definition) {
    return new self(
      $configuration,
      $pluginId,
      $definition,
      $container->get('database')
    );
  }

  /**
   * @return array[]
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  public function build(): array {
    $build = [
      '#attributes' => [
        'class' => ['news-archive-block'],
      ],
      '#cache' => [
        'tags' => [
          'node_list:news',
        ],
      ],
    ];

    $rows = $this->getYearsAndAmounts();
    if ($rows === []) {
      return $build;
    }

    $build['years'] = [
      '#theme' => 'links',
      '#links' => [],
    ];
    foreach ($rows as $row) {
      $year = $row['year'];
      $amount = $row['amount'];
      $build['years']['#links'][] = [
        'title' => "$year ($amount)",
        'url' => Url::fromRoute('view.news.news', ['arg_0' => $year]),
      ];
    }
    return $build;
  }

  /**
   * @return array[]
   */
  private function getYearsAndAmounts(): array {
    $select = $this->database->select('node_field_data', 'nfd');
    $select->addExpression('YEAR(FROM_UNIXTIME(nfd.created))', 'year');
    $select->addExpression('count(*)', 'amount');
    $select->condition('status', (string) NodeInterface::PUBLISHED);
    $select->condition('type', 'news');
    $select->groupBy('year');
    $select->orderBy('year', 'DESC');

    $result = $select->execute();
    if ($result === NULL) {
      return [];
    }
    return $result->fetchAll(PDO::FETCH_ASSOC);
  }

}
