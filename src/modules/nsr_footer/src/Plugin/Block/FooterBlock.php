<?php

namespace Drupal\nsr_footer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use InvalidArgumentException;
use function array_filter;
use function array_pop;
use function explode;
use function next;

/**
 * @Block(
 *  id = "footer_block",
 *  admin_label = @Translation("Footer block"),
 * )
 *
 * Ignore false positives on $form without array as the interface forces this.
 * phpcs:disable Drupal.Commenting.FunctionComment.TypeHintMissing
 */
class FooterBlock extends BlockBase {

  private const AMOUNT_OF_COLUMNS = 3;
  private const ADD = 'add';
  private const REMOVE = 'remove';
  private const ENTRIES = 'entries';
  private const TITLE = 'title';
  private const URL = 'url';
  private const COLOPHON = 'colophon';
  private const SITEMAP = 'sitemap';
  private const AMOUNT_OF_SITEMAPS = 4;

  /**
   * @return array[]
   */
  public function defaultConfiguration(): array {
    return [
      $this->getColumnId(0) => [],
      $this->getColumnId(1) => [],
      $this->getColumnId(2) => [],
      self::COLOPHON => [],
      self::SITEMAP => [],
    ];
  }

  /**
   * @param array[] $form
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *
   * @return array[]
   */
  public function blockForm($form, FormStateInterface $formState): array {
    $form['#tree'] = TRUE;

    $this->updateNumberOfEntries($formState);

    for ($column = 0; $column < self::AMOUNT_OF_COLUMNS; $column++) {
      $columnId = $this->getColumnId($column);
      $columnWrapperId = "column-$column-wrapper";
      $form[$columnId] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Column') . ' ' . $column,
        '#prefix' => "<div id=\"$columnWrapperId\">",
        '#suffix' => '</div>',
      ];

      $entries = $this->configuration[$columnId];
      foreach ($entries as $key => $entry) {
        /* @phpstan-ignore-next-line */
        $form[$columnId][self::ENTRIES][$key] = [
          self::TITLE => [
            '#type' => 'textfield',
            '#title' => $this->t('Link') . ' ' . $key,
            '#placeholder' => $this->t('Link title'),
            '#default_value' => $entry[self::TITLE] ?? '',
          ],
          self::URL => [
            '#type' => 'url',
            '#placeholder' => $this->t('Link path'),
            '#default_value' => $entry[self::URL] ?? '',
          ],
        ];
      }

      /* @phpstan-ignore-next-line */
      $form[$columnId]['actions'][self::ADD] = [
        '#type' => 'button',
        '#name' => self::ADD . '-' . $column,
        '#value' => $this->t('Add link'),
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => [$this, 'columnWrapper'],
          'wrapper' => $columnWrapperId,
        ],
      ];

      if ($entries !== []) {
        $form[$columnId]['actions'][self::REMOVE] = [
          '#type' => 'button',
          '#name' => self::REMOVE . '-' . $column,
          '#value' => $this->t('Remove link'),
          '#limit_validation_errors' => [],
          '#ajax' => [
            'callback' => [$this, 'columnWrapper'],
            'wrapper' => $columnWrapperId,
          ],
        ];
      }
    }

    $colophon = $this->configuration[self::COLOPHON];
    $form[self::COLOPHON] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Colophon link'),
    ];
    $form[self::COLOPHON][self::TITLE] = [
      '#type' => 'textfield',
      '#placeholder' => $this->t('Title'),
      '#default_value' => $colophon[self::TITLE] ?? '',
    ];
    $form[self::COLOPHON][self::URL] = [
      '#type' => 'textfield',
      '#placeholder' => $this->t('Path'),
      '#default_value' => $colophon[self::URL] ?? '',
    ];

    $sitemap = $this->configuration[self::SITEMAP];
    $form[self::SITEMAP] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Sitemap links'),
    ];
    for ($i = 0; $i < self::AMOUNT_OF_SITEMAPS; $i++) {
      $form[self::SITEMAP][$i][self::TITLE] = [
        '#type' => 'textfield',
        '#title' => $this->t('Link') . ' ' . $i,
        '#placeholder' => $this->t('Title'),
        '#default_value' => $sitemap[$i][self::TITLE] ?? '',
      ];
      $form[self::SITEMAP][$i][self::URL] = [
        '#type' => 'textfield',
        '#placeholder' => $this->t('Path'),
        '#default_value' => $sitemap[$i][self::URL] ?? '',
      ];
    }

    return $form;
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $formState
   */
  private function updateNumberOfEntries(FormStateInterface $formState): void {
    $input = &$formState->getUserInput();
    if (
      empty($input['_drupal_ajax']) ||
      !isset($input['_triggering_element_name']) ||
      !$formState->isRebuilding()
    ) {
      return;
    }

    $buttonArray = explode('-', $input['_triggering_element_name']);
    $method = reset($buttonArray);
    $column = (int) next($buttonArray);
    $columnId = $this->getColumnId($column);

    $entries = $input['settings'][$columnId][self::ENTRIES];
    if ($method === self::ADD) {
      $entries[] = [
        self::TITLE => '',
        self::URL => '',
      ];
      $this->configuration[$columnId] = $entries;
    }
    elseif ($method === self::REMOVE) {
      array_pop($entries);
      $this->configuration[$columnId] = $entries;
    }
  }

  /**
   * @param int $column
   *
   * @return string
   */
  private function getColumnId(int $column): string {
    return 'column_' . $column;
  }

  /**
   * @param array[] $form
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *
   * @return array[]
   */
  public function columnWrapper(array $form, FormStateInterface $formState): array {
    $button = $formState->getTriggeringElement();
    if (!isset($button['#name'])) {
      return [];
    }
    $namePieces = explode('-', $button['#name']);
    $column = (int) next($namePieces);
    $columnId = $this->getColumnId($column);

    return $form['settings'][$columnId];
  }

  /**
   * @param array[] $form
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function blockValidate($form, FormStateInterface $formState): void {
    if (!$formState->isSubmitted()) {
      return;
    }
    $input = $formState->getUserInput()['settings'];

    for ($column = 0; $column < self::AMOUNT_OF_COLUMNS; $column++) {
      $columnId = $this->getColumnId($column);
      foreach ($input[$columnId][self::ENTRIES] as $key => $entry) {
        if ($this->invalidLink($entry)) {
          $name = "$columnId][" . self::ENTRIES . "][$key";
          $formState->setErrorByName($name, $this->t('Both title and URL of a link should be filled in.'));
        }
      }
    }

    if ($this->invalidLink($input[self::COLOPHON])) {
      $formState->setErrorByName(self::COLOPHON, $this->t('Both title and URL of a link should be filled in.'));
    }

    foreach ($input[self::SITEMAP] as $key => $entry) {
      if ($this->invalidLink($entry)) {
        $name = self::SITEMAP . "][$key";
        $formState->setErrorByName($name, $this->t('Both title and URL of a link should be filled in.'));
      }
    }
  }

  /**
   * @param string[] $link
   *
   * @return bool
   */
  private function invalidLink(array $link): bool {
    return ($link[self::TITLE] === '' xor $link[self::URL] === '') || $this->invalidUri($link[self::URL]);
  }

  /**
   * @param string $uri
   *
   * @return bool
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  private function invalidUri(string $uri): bool {
    if ($uri === '') {
      return FALSE;
    }

    try {
      if ($uri[0] === '/') {
        $uri = 'internal:' . $uri;
      }
      Url::fromUri($uri);
      return FALSE;
    }
    catch (InvalidArgumentException $exception) {
      return TRUE;
    }
  }

  /**
   * @param array[] $form
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function blockSubmit($form, FormStateInterface $formState): void {
    for ($column = 0; $column < self::AMOUNT_OF_COLUMNS; $column++) {
      $columnId = $this->getColumnId($column);
      $entries = $formState->getValue([$columnId, self::ENTRIES]);
      if (is_array($entries)) {
        $entries = array_filter($entries, static function (array $entry) {
          return $entry[self::TITLE] !== '';
        });
        $this->configuration[$columnId] = $entries;
      }
    }

    $this->configuration[self::COLOPHON] = $formState->getValue(self::COLOPHON);
    $this->configuration[self::SITEMAP] = $formState->getValue(self::SITEMAP);
  }

  /**
   * @return string[]|array[]
   */
  public function build(): array {
    return [
      '#theme' => 'nsr_footer_block',
      '#columns' => [
        $this->configuration[$this->getColumnId(0)],
        $this->configuration[$this->getColumnId(1)],
        $this->configuration[$this->getColumnId(2)],
      ],
      '#colophon' => $this->configuration[self::COLOPHON],
      '#sitemap' => $this->configuration[self::SITEMAP],
    ];
  }

}
