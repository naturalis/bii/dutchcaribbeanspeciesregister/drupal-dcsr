services:
  traefik:
    image: registry.gitlab.com/naturalis/bii/dutchcaribbeanspeciesregister/drupal-dcsr/traefik:${IMAGE_VERSION:?Variable IMAGE_VERSION is empty}
    environment:
      AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID:?Variable AWS_ACCESS_KEY_ID is empty}
      AWS_SECRET_ACCESS_KEY: ${AWS_SECRET_ACCESS_KEY:?Variable AWS_SECRET_ACCESS_KEY is empty}
    volumes:
      - /opt/project/drupal-dcsr/letsencrypt:/letsencrypt:rw

  docker-proxy:
    image: registry.gitlab.com/naturalis/bii/dutchcaribbeanspeciesregister/drupal-dcsr/docker-proxy:${IMAGE_VERSION:?}

  nginx:
    image: registry.gitlab.com/naturalis/bii/dutchcaribbeanspeciesregister/drupal-dcsr/nginx:${IMAGE_VERSION:?}
    volumes:
      - /data/drupal-dcsr/files:/var/www/public/sites/default/files:ro
    labels:
      - traefik.http.routers.arrive-http.entrypoints=web
      - traefik.http.routers.arrive-http.rule=HostRegexp(`.*dutchcaribbeanspecies.org`)
      - traefik.http.routers.arrive-http.middlewares=redirect-to-https
      - traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https
      - traefik.http.middlewares.redirect-to-https.redirectscheme.permanent=true
      # Production
      - traefik.http.routers.dcsr.entrypoints=websecure
      - traefik.http.routers.dcsr.tls=true
      - traefik.http.routers.dcsr.tls.certresolver=route53
      - traefik.http.routers.dcsr.rule=(Method(`GET`) || Method(`POST`)) && Host(`www.dutchcaribbeanspecies.org`)
      - traefik.http.routers.dcsr.priority=1
      # Production c
      - traefik.http.routers.dcsr_cd.entrypoints=websecure
      - traefik.http.routers.dcsr_cd.tls=true
      - traefik.http.routers.dcsr_cd.tls.certresolver=route53
      - traefik.http.routers.dcsr_cd.rule=(Method(`GET`) || Method(`POST`)) && (Host(`www-c.dutchcaribbeanspecies.org`) || Method(`www-d.dutchcaribbeanspecies.org`))
      - traefik.http.routers.dcsr_cd.middlewares=secured
      - traefik.http.routers.dcsr_cd.priority=1
      - traefik.http.middlewares.secured.basicauth.users=sesam:$$apr1$$UlaRc.GC$$pSWwEE446XonlSCl0YZn7/
      # Production naked
      - traefik.http.routers.dcsr_naked.entrypoints=websecure
      - traefik.http.routers.dcsr_naked.tls=true
      - traefik.http.routers.dcsr_naked.tls.certresolver=route53
      - traefik.http.routers.dcsr_naked.rule=(Method(`GET`) || Method(`POST`)) && Host(`dutchcaribbeanspecies.org`)
      - traefik.http.routers.dcsr_naked.middlewares=naked
      - traefik.http.middlewares.naked.redirectregex.regex=^https://dutchcaribbeanspecies.org/(.*)
      - traefik.http.middlewares.naked.redirectregex.replacement=https://www.dutchcaribbeanspecies.org/$${1}
      - traefik.http.middlewares.naked.redirectregex.permanent=true
      # Production linnaeus template
      - traefik.http.routers.dcsr_cd_lt.entrypoints=websecure
      - traefik.http.routers.dcsr_cd_lt.tls=true
      - traefik.http.routers.dcsr_cd_lt.tls.certresolver=route53
      - traefik.http.routers.dcsr_cd_lt.rule=Method(`GET`) && Path(`/linnaeus_template`)
    logging:
      driver: "syslog"
      options:
         syslog-address: "udp://127.0.0.1:5531"
         tag: "nginx"

  exporter:
    image: quay.io/martinhelmich/prometheus-nginxlog-exporter:v1.9.2
    restart: always
    command:
      - -config-file
      - /exporter/nginx-exporter.hcl
    volumes:
      - /opt/project/drupal-dcsr/exporter/:/exporter/
    ports:
      - 4040:4040
      - 5531:5531/udp

  php-fpm:
    image: registry.gitlab.com/naturalis/bii/dutchcaribbeanspeciesregister/drupal-dcsr/php-fpm:${IMAGE_VERSION:?}
    environment:
      MYSQL_DATABASE: ${MYSQL_DATABASE:?Variable MYSQL_DATABASE is empty}
      MYSQL_USER: ${MYSQL_USER:?Variable MYSQL_USER is empty}
      MYSQL_PASSWORD: ${MYSQL_PASSWORD:?Variable MYSQL_PASSWORD is empty}
      DRUPAL_SALT: ${DRUPAL_SALT:?Variable DRUPAL_SALT is empty}
    volumes:
      - /data/drupal-dcsr/files:/var/www/public/sites/default/files:rw

  memcached:
    image: registry.gitlab.com/naturalis/bii/dutchcaribbeanspeciesregister/drupal-dcsr/memcached:${IMAGE_VERSION:?}

  database:
    image: registry.gitlab.com/naturalis/bii/dutchcaribbeanspeciesregister/drupal-dcsr/database:${IMAGE_VERSION:?}
    environment:
      MYSQL_DATABASE: ${MYSQL_DATABASE:?}
      MYSQL_USER: ${MYSQL_USER:?}
      MYSQL_PASSWORD: ${MYSQL_PASSWORD:?}
      MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD:?}
    healthcheck:
      test: mysql --user=$${MYSQL_USER:?} --password=$${MYSQL_PASSWORD:?} -e "connect $${MYSQL_DATABASE:?}"
    volumes:
      - /data/drupal-dcsr/database/init:/docker-entrypoint-initdb.d:ro
      - /data/drupal-dcsr/database/storage:/var/lib/mysql:rw
