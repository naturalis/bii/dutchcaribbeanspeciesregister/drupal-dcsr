# Dutch Caribbean Species Register

Drupal setup for [Dutch Caribbean Species Register: Overview of Dutch Caribbean
biodiversity](https://www.dutchcaribbeanspecies.org/).

## Changes

Read the commit log ([develop](https://gitlab.com/naturalis/bii/dutchcaribbeanspeciesregister/drupal-dcsr/-/commits/develop)
or [master](https://gitlab.com/naturalis/bii/dutchcaribbeanspeciesregister/drupal-dcsr/-/commits/master))
to see recent changes and additions to the website.

## Development

### How to set up

Follow the steps below to set up the development environment. Make sure port 80
& 443 are available for the containers, so stop other web servers before you
start this up.

#### Required software

 1. PHP (preferably same major.minor version as the php-fpm docker container, see docker/php-fpm/Dockerfile)
 2. Composer ([latest](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos))
 3. docker ([latest](https://docs.docker.com/engine/install/))
 4. docker compose ([latest](https://docs.docker.com/compose/install/))
 5. nodejs ([latest](https://nodejs.org/en/download/package-manager/) or with [nvm](https://github.com/nvm-sh/nvm))

#### Development set up

 1. Run `composer install` in the project directory (or with `--ignore-platform-reqs` if you're missing some php extensions)
 2. Run `docker login registry.gitlab.com` ([see GitLab documentation](https://gitlab.com/help/user/packages/container_registry/index#authenticating-to-the-gitlab-container-registry))
 3. Run `docker compose up` in a different cli tab (to see the log output right away)
 4. Run `chmod 777 data/files` to make the files share writable (use `sudo` when you get a permission error)
 5. Add `127.0.0.1 dcsr.dryrun.link` to your hosts file (/etc/hosts)
 6. Open [dcsr.dryrun.link](https://dcsr.dryrun.link) in your favorite browser

If you want to use the correct certificate, use the *traefik-route53-dryrun.link* secret
from bitwarden.

#### Add the linnaeus component

 1. Clone the [linnaeus repository](https://gitlab.com/naturalis/bii/linnaeus/linnaeus_ng)
 2. Follow the instructions in the readme
 3. Focus on the [integrated](https://gitlab.com/naturalis/bii/linnaeus/linnaeus_ng#integrated) part of the instructions

If everything is setup correctly you should see something like this when running `docker ps`:

```
❯ docker ps
CONTAINER ID   IMAGE                                                                        COMMAND                  CREATED          STATUS                             PORTS                                                                      NAMES
2ab84a584cf5   registry.gitlab.com/naturalis/bii/linnaeus/linnaeus_ng/nginx:develop         "/docker-entrypoint.…"   10 seconds ago   Up 9 seconds                       80/tcp                                                                     linnaeus_ng_nginx_1
2888c21ac7cd   registry.gitlab.com/naturalis/bii/linnaeus/linnaeus_ng/php-fpm:develop       "docker-php-entrypoi…"   11 seconds ago   Up 9 seconds                       9000/tcp                                                                   linnaeus_ng_php-fpm_1
c7b9068b0814   registry.gitlab.com/naturalis/bii/linnaeus/linnaeus_ng/database:develop      "docker-entrypoint.s…"   12 seconds ago   Up 10 seconds (health: starting)                                                                              linnaeus_ng_database_1
781b429913f6   registry.gitlab.com/naturalis/bii/drupal/docker-drupal/traefik:latest        "/entrypoint.sh --gl…"   56 seconds ago   Up 54 seconds                      0.0.0.0:80->80/tcp, :::80->80/tcp, 0.0.0.0:443->443/tcp, :::443->443/tcp   drupal-dcsr_traefik_1
918a3d6b4e85   registry.gitlab.com/naturalis/bii/drupal/docker-drupal/nginx:latest          "/docker-entrypoint.…"   57 seconds ago   Up 55 seconds                      80/tcp                                                                     drupal-dcsr_nginx_1
3235c44f97e1   registry.gitlab.com/naturalis/bii/drupal/docker-drupal/php-dev:latest        "docker-php-entrypoi…"   57 seconds ago   Up 56 seconds                      9000/tcp                                                                   drupal-dcsr_php-fpm_1
b57492e6864e   registry.gitlab.com/naturalis/bii/drupal/docker-drupal/mariadb:latest        "docker-entrypoint.s…"   58 seconds ago   Up 56 seconds (healthy)                                                                                       drupal_dcsr_database_1
dfe5e0bd9bd7   registry.gitlab.com/naturalis/bii/drupal/docker-drupal/memcached:latest      "docker-entrypoint.s…"   58 seconds ago   Up 56 seconds                                                                                                 drupal-dcsr_memcached_1
9a952a70bcab   registry.gitlab.com/naturalis/bii/drupal/docker-drupal/docker-proxy:latest   "/docker-entrypoint.…"   58 seconds ago   Up 56 seconds                                                                                                 drupal-dcsr_docker-proxy_1
```

And the linnaeus part of the site should be reachable through [dcsr.dryrun.link/linnaeus_ng/](https://dcsr.dryrun.link/linnaeus_ng/).
If you do not see anything (white screen), be sure to set the template skin setting in linnaeus
to *linnaeus_ng_integrated*.


#### Xdebug set up

 1. Turn on `Start listening for PHP Debug Connections` in the Run menu (PhpStorm)
 2. Turn on `Break at first line in PHP scripts` temporarily in the Run menu (PhpStorm)
 3. Refresh [dcsr.dryrun.link](https://dcsr.dryrun.link) in your favorite browser
 4. Go back to PhpStorm, the path mapping should have automatically popped up
 5. Map `/var/www` to the project main directory

#### Frontend asset generation

 1. Go to `src/themes/nsr` from the project directory
 2. Run `npm install`
 3. Run `npm run build` to generate frontend assets in the `dist` directory
 4. Read the `package.json` file for more commands that are useful when developing

### Static code analysis

This project uses a variation of tools to make the code as uniform and readable
as possible. When you run the `composer test` command, you can locally check if
you made any errors. On GitLab CI the `composer ci` command will run on every
merge request created, to check if the code is according to our standards.
Check out the commands for their current tools and configuration.

 - [phpcbf](https://phpqa.io/projects/phpcbf.html) PHP Code Beautifier and Fixer fixes violations of a defined coding
   standard.
 - [phpcs](https://phpqa.io/projects/phpcs.html) PHP_CodeSniffer detects violations of a defined set of coding
   standards.
 - [phplint](https://phpqa.io/projects/php-parallel-lint.html) PHP Parallel Lint checks the syntax of PHP files.
 - [phpmd](https://phpqa.io/projects/phpmd.html) PHP Mess Detector scans PHP source code and looks for potential
   problems such as possible bugs, dead code, suboptimal code, and overcomplicated expressions.
 - [phpstan](https://phpqa.io/projects/phpstan.html) PHP Static Analysis Tool focuses on finding errors in your code
   without actually running it.

### Deployment

This project uses ansible to deploy the latest code on the `develop` branch to
the acceptance environment. See the `ansible` directory in this project for the
current configuration used by ansible.

#### Setup role

The setup role creates two users and groups that are needed for the php-fpm and
database container shares. This is done to increase the security in and outside
the container. To make this possible those users and groups need a static uid &
guid. Those ids could conflict on a new hosting environment. This issue can be
fixed by changing the conflicting user/group on the host to a different id. The
ansible created users and groups cannot be easily changed as they are hard set
in the docker image.

### Config export

Config changes should be done by changing or adding it in the development
environment, but not by hacking the acc/prod environment. When the change is
done in the development environment, log in to the php-fpm container with the
following command in the project root `docker compose exec php-fpm sh`, after
you logged in you can run `./vendor/bin/drush config:export` (`cex`). Check if
the changes are expected, before entering `y` to complete the command. This
will export changes to the `config/sync` directory. Logout the php-fpm
container and check the changes before committing them. For instance sometime
there are labels reverted to the English translation instead of Dutch, just
reset those specific changes. Create a new branch and commit the changes.
Create a PR/MR to run the pipeline. When everything is green the MR can be
merged and will be automatically deployed to acc. Verify if the config changes
are visible as expected.

### Composer patches

Sometimes a package require in the `composer.json` file has a bug. This can be
fixed by adding a patch to the `extra -> patches -> [package]` section. The
`composer/installers` package will make sure those patches are applied after
installing the given package. After a while the bugs will usually get fixed, so
the patch will not apply anymore.  You will see a warning when running
`composer install/update`. Just remove the package from the `composer.json` and
run `composer update lock` to reinstall the package in the current version
without the patch en update the lock file to reflect the missing patch.

## Trivyignore

The gitlab pipeline checks the security of the build docker images using
[trivy](https://trivy.dev/).
Whenever trivy throws up security issues that stop the pipeline but
can't be fixed yet you can add a CVE number of the issue to the
ignore file.
This process has been centralized in
[lib / trivyignore](https://gitlab.com/naturalis/lib/trivyignore),
you should follow the
[instructions](https://gitlab.com/naturalis/lib/trivyignore#procedure) in the project.

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

 * `pre-commit autoupdate`
 * `pre-commit install`

