#!/bin/sh
set -eu

image_version=build_$CI_COMMIT_REF_SLUG

mkdir -p data/database/storage
chown 999:999 data/database/storage

mkdir -p data/files/fixtures
chown 82:82 data/files

docker pull "$CI_REGISTRY_IMAGE/traefik:$image_version"
docker pull "$CI_REGISTRY_IMAGE/docker-proxy:$image_version"
docker pull "$CI_REGISTRY_IMAGE/nginx:$image_version"
docker pull "$CI_REGISTRY_IMAGE/php-fpm:$image_version"
docker pull "$CI_REGISTRY_IMAGE/memcached:$image_version"
docker pull "$CI_REGISTRY_IMAGE/database:$image_version"
docker run --rm -v "$PWD":"$PWD":ro -v /var/run/docker.sock:/var/run/docker.sock:ro  -e "IMAGE_VERSION=$image_version" \
  docker compose -f "$PWD/docker-compose.yml" -f "$PWD/overrides/docker-compose.ci.yml" up -d
