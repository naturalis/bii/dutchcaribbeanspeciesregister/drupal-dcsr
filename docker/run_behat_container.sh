#!/bin/sh
set -eu

# Disable monolog.
rm config/monolog.services.yml

docker create --name drupal-dcsr_behat --network drupal-dcsr_web -e MYSQL_DATABASE=develop -e MYSQL_USER=develop \
  -e MYSQL_PASSWORD=develop -e DRUPAL_SALT=hash_salt -e COMPOSER_HOME=/var/www/.composer/ -v "$PWD":/var/www \
  -v "$PWD/data/files":/var/www/public/sites/default/files \
  registry.gitlab.com/naturalis/bii/drupal/docker-drupal/php-build:latest sh -c \
  "set -eu
  composer install --no-interaction
  getent hosts traefik | awk '{ print \$1\" dev.nederlandsesoorten.nl\" }' >> /etc/hosts

  count=0
  while ! ./vendor/bin/drush status --fields=Drupal bootstrap | grep Successful; do
    sleep 5
    count=\$count+1
    if [ \$count = 6 ]; then
      echo Failed to bootstrap Drupal
      exit 1
    fi
  done

  vendor/bin/drush updatedb -y
  vendor/bin/drush config:import -y
  vendor/bin/drush cache:rebuild
  vendor/bin/behat --config ./qa/behat/behat.yml --colors --strict ./qa/behat/features"

docker network connect drupal-dcsr_backend drupal-dcsr_behat
docker start --attach drupal-dcsr_behat
